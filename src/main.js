import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
import mind from '@/../modules/mind-core-vue'

Vue.use(mind)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
