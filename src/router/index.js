import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Inicio',
    component: Home,
		meta: {
			view: {
				title: 'Inicio',
			},
			nav: {
				icon: 'el-icon-s-home',
			},
		}
  },
  {
		path: '/troncales',
		name: 'Troncales',
		component: () => import(/* webpackChunkName: "trunks" */ '../views/Troncales.vue'),
		meta: {
			view: {
				title: 'Administración de Troncales',
			},
			nav: {
				icon: 'el-icon-tickets',
			},
		},
		children:[
			{
				path: ':id',
				component: () => import(/* webpackChunkName: "trunks" */ '../views/Troncal.vue'),
				meta: {
					view: {
						title: 'Propiedades de la troncal',
						titleNew: 'Nueva Troncal',
					},
				}
			}
		]
  },
  {
		path: '/modulos',
		name: 'Módulos',
		component: () => import(/* webpackChunkName: "modules" */ '../views/Modules.vue'),
		meta: {
			view: {
				title: 'Módulos Asterisk',
			},
			nav: {
				icon: 'el-icon-cpu',
			},
		},
  },
  {
		path: '/eventos',
		name: 'Eventos',
		component: () => import(/* webpackChunkName: "events" */ '../views/Events.vue'),
		meta: {
			view: {
				// title: 'Administración de Clientes',
			},
			nav: {
				icon: 'el-icon-tickets',
			},
		},
  },
]

const router = new VueRouter({
  routes
})

export default router
