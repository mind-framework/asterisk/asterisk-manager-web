if [ -z "$ENVIRONMENT" ]; then
	ENVIRONMENT='latest'
fi
echo "Inciando Entorno '$ENVIRONMENT'"

echo "STAGE=$ENVIRONMENT" >> /usr/share/nginx/html/.env
ln -sf /usr/share/nginx/html /usr/share/nginx/html$PUBLIC_URL
nginx -g "daemon off;"
