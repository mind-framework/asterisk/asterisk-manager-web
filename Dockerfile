FROM nginx:stable-alpine
EXPOSE 80 443
VOLUME [ "/etc/nginx/conf.d" ]
ADD conf/nginx/conf.d/* /etc/nginx/conf.d/
ADD dist /usr/share/nginx/html
ADD entrypoint.sh entrypoint.sh
CMD ["./entrypoint.sh"]